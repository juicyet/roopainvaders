package ee.home.roopa;



public class Parameters {
	
	//----------------------------------------
	// klahvi-settingute konstandid
	public static int MOVEMENT_ARROWS = 0;
	public static int MOVEMENT_GYRO = 1;
	public static int MOVEMENT_TOUCH = 2;
	
	public static int NUKE_DOWNARROW = 0;
	public static int NUKE_TOUCH = 1;
	
	public static int SHOOTKEY_UPARROW = 0;
	public static int SHOOTKEY_TOUCH = 1;
	
	public static int WEAPON_SINGLEBULLET = 0;
	public static int WEAPON_DUALBULLET = 1;
	public static int WEAPON_SPLASHER = 2;
	public static int WEAPON_MACHINEGUN = 3;
	public static int mainWeapon = WEAPON_SINGLEBULLET;
	//----------------------------------------	
	
	public static int DUAL_BULLET_DROP_PROBABILITY = 7;
	public static int SPLASHER_DROP_PROBABILITY = 7;
	public static int MG_DROP_PROBABILITY = 7;
	public static int LIFE_DROP_PROBABILITY = 2;
	
	public static int lives = 3;
	public static int maxLives = 5;
	public static int INITIAL_ENEMY_COUNT = 48;
	
	public static int SHOOT_INTERVAL = 1000;
	public static int SHOOT_NUKE_INTERVAL = 10000;
	public static int BLEEDING_INTERVAL = 2000;
	
	public static int ENEMY_SHOOTING_PROBABILITY = 60;
	public static float BADASS_BULLET_HORIZONTAL_SPEED = 1;
	public static float BADASS_SHIELD_SIZE = 5;
	
	public static boolean initialOnSizeChanged = true;
	public static int enemyCount;
	public static float enemySpeed = (float) 0.25;
	public static AppState appState = AppState.Starting;
	public static int level = 1;
	public static int score = 0;
	
	public static int movementKeys = MOVEMENT_ARROWS;
	public static int nukeKey = NUKE_DOWNARROW;
	public static int shootKey = SHOOTKEY_UPARROW;
	
	public static int TRACK_NUMBER = 2;
	public static boolean NO_MENU_MUSIC = true;
	
	
	// Iceman, �ra seda publikuks v��na! ma tahan et seda ainut getteeriga saaks k�sida.
	private static float levelupEnemySpeedAdjustment = (float) (level * 0.25);// how much faster will the enemies go with each level
	
	public static void InitTestParameters() {
		INITIAL_ENEMY_COUNT = 17;
		SHOOT_INTERVAL = 100;
		SHOOT_NUKE_INTERVAL = 2000;
		lives = 1;
	}
	
	public static void InitTarmoParameters() {
		INITIAL_ENEMY_COUNT = 48;
		SHOOT_INTERVAL = 300;
		SHOOT_NUKE_INTERVAL = 8000;
		BADASS_BULLET_HORIZONTAL_SPEED = 1;
		
		DUAL_BULLET_DROP_PROBABILITY = 15;
		SPLASHER_DROP_PROBABILITY = 30;
		MG_DROP_PROBABILITY = 25;
		LIFE_DROP_PROBABILITY = 45;
	}
	
	public static void InitIcemanParameters() {
		INITIAL_ENEMY_COUNT = 48;
		SHOOT_INTERVAL = 1000;
		SHOOT_NUKE_INTERVAL = 10000;
		
		DUAL_BULLET_DROP_PROBABILITY = 15;
		SPLASHER_DROP_PROBABILITY = 30;
		MG_DROP_PROBABILITY = 25;
		LIFE_DROP_PROBABILITY = 45;		
		lives = 3;
	}

	//-------------------------------
	
	// how much faster will the enemies go with each level
	public static float getLevelupEnemySpeedAdjustment() {
		return levelupEnemySpeedAdjustment;
	}
}
