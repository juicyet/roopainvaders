package ee.home.roopa;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Spinner;

public class et extends Activity {
	
	MyView mMyView;
	CreditsView mCreditsView;
	
	static MediaPlayer activeMusic = null;
	
	/** when the activity is first created. */
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    	
        // initialize full screen view
        requestWindowFeature(Window.FEATURE_NO_TITLE);  
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        initMenus();       
    }

	private class MusicBoxCheckedListener implements OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
			stopMusic();
			if (!isChecked) {
				// play music
				Parameters.NO_MENU_MUSIC = false;
				startMenuMusic();
			}
			else {
				// don't play music
				Parameters.NO_MENU_MUSIC = true;				
			}
		}		
	}
	
	void terminate() {
		stopMusic();
		Parameters.appState = AppState.Terminated;
		if (this.mMyView != null) {
			this.mMyView.clearLists();
		}
		super.onDestroy();		
		this.finish();
	}
	
	void startMenuMusic() {
		activeMusic = MediaPlayer.create(this, R.raw.menu);
		activeMusic.setLooping(true);
		activeMusic.start();
	}
	
	void initMenus() {
		setContentView(R.layout.main);
		
		CheckBox musicBox = (CheckBox)findViewById(R.id.muteSounds);
		
		// setting checkbox visible value
		musicBox.setChecked(Parameters.NO_MENU_MUSIC);
		
		musicBox.setOnCheckedChangeListener(new MusicBoxCheckedListener());
		try {
			stopMusic();
			
			// if "play menu music" checkbox is enabled, start menu music
			Parameters.NO_MENU_MUSIC = musicBox.isChecked();
			if (!musicBox.isChecked()) {				
				startMenuMusic();
			}
			
		}
		catch (Exception e) {
			//System.out.println("heli loomine ebaġnnestus. ignoreerime :)" + e);
		}
		
        if (this.mMyView == null) {
        	this.mMyView = new MyView(this);
        }
        if (this.mCreditsView == null) {
        	this.mCreditsView = new CreditsView(this);
        }
        
        Spinner s = (Spinner) findViewById(R.id.spinner);
        @SuppressWarnings("rawtypes")
		ArrayAdapter adapter = ArrayAdapter.createFromResource(
                this, R.array.soundtracks, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s.setAdapter(adapter);
        s.setSelection(Parameters.TRACK_NUMBER);
		final Button button = (Button)findViewById(R.id.btnNewGame);
		if (Parameters.appState == AppState.Paused) {
			button.setText("Continue");
		}
		else {
			button.setText("New Game");
		}		
		
		OnClickListenerForStartButton startBtnListener = new OnClickListenerForStartButton();
		startBtnListener.setActivity(this);
		button.setOnClickListener(startBtnListener);
		
		final Button creditsButton = (Button)findViewById(R.id.btnCredits);			
		creditsButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				setContentView(mCreditsView);
			}
		});
		
		final Button exitButton = (Button)findViewById(R.id.btnExit);			
		exitButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				terminate();
			}
		});	
	}
	
	public static void stopMusic() {
		if(activeMusic != null) {
			activeMusic.stop();
			activeMusic = null;
		}
	}
	
	private class OnClickListenerForStartButton implements OnClickListener {
		
		Activity activity;
		
		public void onClick(View v) {
			try {
				stopMusic();
				
				Spinner s = (Spinner) findViewById(R.id.spinner);
				String selectedMusic = (String) s.getSelectedItem();
		        				
				if (selectedMusic.equalsIgnoreCase("alvin1")) {
					Parameters.TRACK_NUMBER = 0;
					activeMusic = MediaPlayer.create(this.activity, R.raw.combat1);
				}
				else if (selectedMusic.equalsIgnoreCase("alvin2")) {
					Parameters.TRACK_NUMBER = 1;
					activeMusic = MediaPlayer.create(this.activity, R.raw.combat2);
				}
				/*
				else if (selectedMusic.equalsIgnoreCase("none")) {
					// do nothing
					activeMusic = null;
				}
				*/
				if (activeMusic != null) {
					activeMusic.setLooping(true);
					activeMusic.start();
				}
			}
			catch (Exception e) {
				//System.out.println("midagi ebaġnnestus. ignoreerime :)" + e);
			}
						
			// start or continue the game
			if (Parameters.appState == AppState.GameOver) {
				mMyView = new MyView(activity);
				mMyView.reset();
				setContentView(mMyView);
				mMyView.resume();
			}
			else {
				setContentView(mMyView);
				mMyView.resume();
			}
	    }
		
		public void setActivity(Activity activity) {
			this.activity = activity;
		}
	}
}