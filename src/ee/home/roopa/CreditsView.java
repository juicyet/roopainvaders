package ee.home.roopa;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;


public class CreditsView extends View implements OnTouchListener {
	private ArrayList<String> credits = new ArrayList<String>();
	Rect rect = new Rect();
	private RefreshHandler mRedrawHandler = new RefreshHandler();
	private int frame = 0;
	private Context mContext;
	private boolean canExit = false;
	
	public CreditsView(Context context) {
		super(context);
		this.mContext = context;
		this.setOnTouchListener(this);
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		invalidate();
		update();
		
	};

	@Override
	protected void onDraw(Canvas canvas) {
		credits.add("CREDITS");
		credits.add("---------");
		credits.add("Programming:");
		credits.add("Ylar Luisk");
		credits.add("Tarmo Raudsep");
		credits.add("Ott Madis Ozolit");
		credits.add("Music and soundwork:");
		credits.add("Alvin Tarkmees");
		credits.add("Testing:");
		credits.add("Vanaema");
		credits.add("Sven L");
				
		Paint creditPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		creditPaint.setColor(Color.WHITE);
		
		if (frame >= credits.size()) {	
			frame = credits.size() - 1;
		}
		for (int i = frame;  i >= 0; i--) {
			creditPaint.setTextSize(2 + 3*i);
			creditPaint.getTextBounds(credits.get(i), 0, credits.get(i).length(), rect);
			float textStartX;
			float textStartY;
			textStartX = getWidth() / 2 - rect.width() / 2;
			textStartY = getHeight() / 7 + (rect.height() + 15) * i;
			canvas.drawText(credits.get(i), textStartX, textStartY, creditPaint);
		}		
	};
	
	private void update() {
		frame ++;
		mRedrawHandler.sleep(1000);
	}
	
	
	private class RefreshHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			if (canExit) {
				frame = 0;
				return;
			}
			CreditsView.this.update();
			CreditsView.this.invalidate();
       	
		}
		
		
		public void sleep(long delayMillis) {
			this.removeMessages(0);
			sendMessageDelayed(obtainMessage(0), delayMillis);
		}
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		canExit = true;
		((et)mContext).initMenus();
		return true;
	}	
}
