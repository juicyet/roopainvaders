package ee.home.roopa;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class MyView extends View implements OnTouchListener {
	
	private int mGoingDownCounter = 2;
	
	
	Ship mShip = null;
	private RefreshHandler mRedrawHandler = new RefreshHandler();
	MediaPlayer mBulletSound;
	MediaPlayer nukeLaunchSound;
	
	
	private ArrayList<Bullet> mBullets = new ArrayList<Bullet>();
	private ArrayList<Enemy> mEnemies = new ArrayList<Enemy>();
	private ArrayList<Bullet> mEnemyBullets = new ArrayList<Bullet>();
	private ArrayList<PowerUp> mPowerUps = new ArrayList<PowerUp>();
	
	private int PADDING = 60;
	private int mFD = 1;
	private int goingDown = 0;
		
	boolean canShoot = true;
	boolean canShootNuke = true;
	int canShootCounter = 0;
	int canShootNukeCounter = 0;
	int bleedingCounter = 0;
	boolean enemyCanShoot = true;
	int enemyCanShootCounter = 0;
	
	int SLEEP_TIMEOUT = 40;
	int SHIP_BOTTOM_PADDING = 10;
	
	Paint mFriendlyBulletPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	Paint mNukeExplosionPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	Paint mEnemyBulletPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	Paint mBadassBulletPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	
	Context mContext;
	
	private Nuke mNuke = null;
	private NukeExplosion mNukeExplosion = null;
	
	Drawable shipImage = null;
	Drawable bleedingShipImage = null;
	Drawable enemyImage = null;
	Drawable badassImage = null;
	Drawable dualWeaponPowerupImage = null;
	Drawable splasherWeaponPowerupImage = null;
	Drawable machinegunWeaponPowerupImage = null;
	Drawable lifePowerupImage = null;
	Drawable livesImage = null;
	Drawable nukeMissleImage = null;	
	
	private ArrayList<Drawable> mNukePics = new ArrayList<Drawable>();
	
	int mNukePicIndex = 0;
	
	public MyView(Context context) {
		super(context);
		mContext = context;
		setFocusable(true);
		setFocusableInTouchMode(true);
		
		//PARAMEETRID
		// initialize personalized parameters (bullet pissing, nuke spam, etc...)
		//Parameters.InitTestParameters();
		Parameters.InitIcemanParameters();
		//Parameters.InitTarmoParameters();			
		//--------------------------------
		
		// initialize sounds
		mBulletSound = MediaPlayer.create(context, R.raw.gunshot2);
		nukeLaunchSound = MediaPlayer.create(context, R.raw.nukelaunch);
		
		// initialize colors
		mEnemyBulletPaint.setColor(Color.RED);
		mNukeExplosionPaint.setColor(Color.WHITE);
		mFriendlyBulletPaint.setColor(Color.BLUE);
		mBadassBulletPaint.setColor(Color.rgb(183, 70, 2));
		
		// initialize mothership
		mShip = new Ship();
		mShip.movingSpeed = 5;
		mShip.Laius = 20;
		
		// initialize images
		Resources res = mContext.getResources();
		shipImage = res.getDrawable(R.drawable.ship);
		bleedingShipImage = res.getDrawable(R.drawable.bleedingship);
		enemyImage = res.getDrawable(R.drawable.enemy);
		badassImage = res.getDrawable(R.drawable.enemybad);
		dualWeaponPowerupImage = res.getDrawable(R.drawable.powerupdualweapon);
		splasherWeaponPowerupImage = res.getDrawable(R.drawable.powerupsplasherweapon);
		machinegunWeaponPowerupImage = res.getDrawable(R.drawable.powerupmgweapon);
		lifePowerupImage = res.getDrawable(R.drawable.poweruplife);
		livesImage = res.getDrawable(R.drawable.lives);
		nukeMissleImage = res.getDrawable(R.drawable.nukemissle);
		
		mNukePics.add(res.getDrawable(R.drawable.nukeready));
		mNukePics.add(res.getDrawable(R.drawable.nuke6));
		mNukePics.add(res.getDrawable(R.drawable.nuke5));
		mNukePics.add(res.getDrawable(R.drawable.nuke4));
		mNukePics.add(res.getDrawable(R.drawable.nuke3));
		mNukePics.add(res.getDrawable(R.drawable.nuke2));
		mNukePics.add(res.getDrawable(R.drawable.nuke1));
				
		// register touch listener
		this.setOnTouchListener(this);
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		init(w, h);
	};
	
	public void init(int w, int h) {
		float stepX, stepY;	
		float maxR, maxC;
		if(w < h) {				
			//portrait mode	
			maxC = 6;
			maxR = 8;					
			}
		else {
			//landscape mode
			maxC = 12;
			maxR = 4;
		}		

		mShip.x = this.getWidth() / 2;
		mShip.y = this.getHeight() - mShip.Laius - SHIP_BOTTOM_PADDING;			
		
		stepX = (w - PADDING) / maxC;
		stepY = (h - PADDING - h / 3) / maxR;
				
		
		if (!Parameters.initialOnSizeChanged) {
			int counter = Parameters.enemyCount;
			populateEnemies(maxR, maxC, stepX, stepY, counter);
			Parameters.appState = AppState.Running;
			update();
		}
		else {
			populateEnemies(maxR, maxC, stepX, stepY, Parameters.INITIAL_ENEMY_COUNT);
			Parameters.enemyCount = mEnemies.size();
			Parameters.initialOnSizeChanged = false;
			// set stuff moving
			Parameters.appState = AppState.Running;
			update();
		}
	}
	
	
	private void populateEnemies(float maxR, float maxC, float stepX, float stepY, int counter) {
		
		for(int r = 1; r <= maxR; r++)
		{
			for(int c = 1; c <= maxC; c++){
				if (counter <= 0) break;
				Enemy tmp;
				if(r == 1) {
					tmp = new Enemy(Parameters.enemySpeed, badassImage);
					tmp.enemyShield = Parameters.BADASS_SHIELD_SIZE;
				}				
				else {
					tmp = new Enemy(Parameters.enemySpeed, enemyImage);
				}
				tmp.x = stepX * c;
				tmp.y = stepY * r;
				mEnemies.add(tmp);
				counter --;					
			}
			if (counter <= 0) break;
		}		
	}
	
	private class RefreshHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			if (Parameters.appState == AppState.GameOver || Parameters.appState == AppState.Terminated) {
				return;
			}
			MyView.this.update();
			MyView.this.invalidate();
		}
		
		public void sleep(long delayMillis) {
			this.removeMessages(0);
			sendMessageDelayed(obtainMessage(0), delayMillis);
		}
	}
	
	@Override
	protected void onDraw(android.graphics.Canvas canvas) {	
		
		//meie laeva joonistamine, kui oled elus, siis proceed as normal, kui oled surnud, 
		//saad oma GAME OVER sildiga sirvida nagu korralik n00b muiste ja skoori ka n�ha
		String mScoreString = "Score: "+ Integer.toString(Parameters.score);
		canvas.drawText(mScoreString, 165, 20, mShip.paint);
		
		if (Parameters.appState == AppState.GameOver) {
			canvas.drawText("GAME OVER", mShip.x, mShip.y, mEnemyBulletPaint);			
		}
		
		else if (Parameters.appState == AppState.Running) {
			
			// tuumapommi valmisolemise ikooni joonistamine			
			mNukePics.get(mNukePicIndex).setBounds(getWidth() - 25, 0 , getWidth(), 25);
			mNukePics.get(mNukePicIndex).draw(canvas);
						
			//elude joonistmaine
			for(int i = 0; i < Parameters.lives; i++) {
				livesImage.setBounds(i * 35, 0 , 25 + i * 35, 25);
				livesImage.draw(canvas);
			}			
			
			//tuuma paugu joonistamine
			if(mNukeExplosion != null) {
				canvas.drawCircle(mNukeExplosion.nukeX, mNukeExplosion.nukeY, mNukeExplosion.nukeR, mNukeExplosionPaint);
			}			
			
			//meie laeva joonistamine
			if (mShip.bleeding) {
				bleedingShipImage.setBounds(Math.round(mShip.x), 
						Math.round(mShip.y), 
						Math.round(mShip.x + mShip.Laius), 
						Math.round(mShip.y + mShip.Laius));			
				bleedingShipImage.draw(canvas);
			}
			else {
				shipImage.setBounds(Math.round(mShip.x), 
						Math.round(mShip.y), 
						Math.round(mShip.x + mShip.Laius), 
						Math.round(mShip.y + mShip.Laius));			
				shipImage.draw(canvas);
			}
						
			// tulnukate joonistamine
			for (int i = 0; i < mEnemies.size(); i++) {
				Enemy tmpEnemy = mEnemies.get(i);
				tmpEnemy.image.setBounds(Math.round(tmpEnemy.x), Math.round(tmpEnemy.y), Math.round(tmpEnemy.x + tmpEnemy.width), Math.round(tmpEnemy.y + tmpEnemy.height));
				tmpEnemy.draw(canvas);				
			}
		
			//heade kuulide joonistamine
			for(int i = 0; i < mBullets.size(); i++) {
				Bullet tmp = mBullets.get(i);
				if(tmp instanceof Nuke){
					nukeMissleImage.setBounds(Math.round(tmp.x - tmp.r), 
							Math.round(tmp.y - tmp.r), 
							Math.round(tmp.x + tmp.r), 
							Math.round(tmp.y + tmp.r));
					nukeMissleImage.draw(canvas);
				}
				else {
					canvas.drawCircle(tmp.x, tmp.y, tmp.r, mFriendlyBulletPaint);
				}
			}
			
			// pahade kuulide joonistamine
			for(int i = 0; i < mEnemyBullets.size(); i++) {
				Bullet tmp = mEnemyBullets.get(i);
				if (tmp.isBadass()){
					canvas.drawCircle(tmp.x, tmp.y, tmp.r, mBadassBulletPaint);
				} 
				else {
					canvas.drawCircle(tmp.x, tmp.y, tmp.r, mEnemyBulletPaint);
				}
			}
			
			// powerupide joonistamine
			for(PowerUp powerup : mPowerUps) {
				powerup.image.setBounds(
						Math.round(powerup.x), 
						Math.round(powerup.y), 
						Math.round(powerup.x + powerup.width), 
						Math.round(powerup.y + powerup.height));
				powerup.draw(canvas);
			}
		}
	};
	
	private void playExplosionSound() {
		try {
			MediaPlayer explosionSound;
			explosionSound = MediaPlayer.create(mContext, R.raw.explosion);
			explosionSound.start();
		}
		catch (Exception e) {
			//System.out.println("heli loomine eba�nnestus. ignoreerime :)" + e);
		}	
	}
	
	private void playRickochetSound() {
		try {
			MediaPlayer rickochetSound;
			rickochetSound = MediaPlayer.create(mContext, R.raw.rickochet);
			rickochetSound.start();
		}
		catch (Exception e) {
			//System.out.println("heli loomine eba�nnestus. ignoreerime :)" + e);
		}	
	}
	
	private void update() {
		if (Parameters.appState == AppState.Running) {
			
			// if out of enemies -> level up! & repopulate
			if(mEnemies.size() == 0) {
				goLevelUp();
			}
			
			if (leftArrowIsDown) 
				mShip.x = mShip.x - mShip.movingSpeed;
			
			if (rightArrowIsDown) 
				mShip.x = mShip.x + mShip.movingSpeed;
			
			if(mShip.x <= 0) {
				mShip.x = getWidth() - mShip.Laius;
			}
			else if(mShip.x > getWidth() - mShip.Laius) {
				mShip.x = 0;
			}
			
			Enemy wingman = mEnemies.get(0);
			
			// nelinurga objekt meie laeva jaoks (kasutatakse collisioni avastamisel)
			Rect shipRect = new Rect(
					Math.round(mShip.x), 
					Math.round(mShip.y), 
					Math.round(mShip.x) + mShip.Laius, 
					Math.round(mShip.y) + mShip.Laius);
			
			//k�ib l�bi powerupide listi
			for (Iterator<PowerUp> iter = mPowerUps.iterator(); iter.hasNext();) {
				PowerUp powerUp = iter.next();
				powerUp.y += powerUp.speed;
				
				// nelinurga objekt powerupi objekti jaoks (kasutatakse collisioni avastamisel)
				Rect powerupRect = new Rect(
						Math.round(powerUp.x), 
						Math.round(powerUp.y), 
						Math.round(powerUp.x + powerUp.width), 
						Math.round(powerUp.y + powerUp.height));
				
				if(shipRect.intersect(powerupRect)) {
				
					if (powerUp instanceof DualBulletWeaponPowerUp) {
						Parameters.mainWeapon = Parameters.WEAPON_DUALBULLET;
					}
					else if (powerUp instanceof SplasherBulletWeaponPowerUp) {
						Parameters.mainWeapon = Parameters.WEAPON_SPLASHER;
					}
					else if (powerUp instanceof MgBulletWeaponPowerUp) {
						Parameters.mainWeapon = Parameters.WEAPON_MACHINEGUN;
					}
					else if (powerUp instanceof LivesPowerUp) {
						if(Parameters.lives < Parameters.maxLives) {						
							Parameters.lives ++;
						}							
					}
					iter.remove();
				}
			}
			
			// k�ib l�bi vaenlaste listi
			for(Enemy tmp : mEnemies) {
				if(mFD == 1) {
					tmp.x += tmp.speed;
					if(wingman.x < tmp.x) {
						wingman = tmp;
					}						
				}
				else if(mFD == -1) {
					tmp.x -= tmp.speed;
					if(wingman.x > tmp.x) {
						wingman = tmp;
					}
				}
				else if(mFD == 0) {
					tmp.y += tmp.speed;
				}
				else {
					
				}				
			}			
			
			if(wingman.x >= getWidth() - PADDING / 2 - wingman.width && mFD == 1) {
				mFD = 0;
			}
			else if(wingman.x <= PADDING / 2 && mFD == -1) {
				mFD = 1;
			}
			else if(mFD == 0) {
				goingDown++;
				if(goingDown > mGoingDownCounter) {
					mFD = -1;
					goingDown = 0;
				}
			}
			
			// meie laeva tulistamine
			if (upArrowIsDown && canShoot && !mShip.bleeding) {
				canShoot = false;
				Bullet newBullet;
				if (Parameters.mainWeapon == Parameters.WEAPON_DUALBULLET) {
					newBullet = new Bullet();
					newBullet.x = mShip.x;
					newBullet.y = mShip.y;
					mBullets.add(newBullet);
					
					newBullet = new Bullet();
					newBullet.x = mShip.x + mShip.Laius;
					newBullet.y = mShip.y;
					mBullets.add(newBullet);
					
					mBulletSound.start();
					
				}
				
				else if (Parameters.mainWeapon == Parameters.WEAPON_SPLASHER) {
					newBullet = new Bullet();
					newBullet.x = mShip.x + (mShip.Laius / 2);
					newBullet.y = mShip.y;
					newBullet.horizontalspeed = (float) 0.4 * newBullet.speed;
					mBullets.add(newBullet);
					mBulletSound.start();
					
					newBullet = new Bullet();
					newBullet.x = mShip.x + (mShip.Laius / 2);
					newBullet.y = mShip.y;
					newBullet.horizontalspeed = (float) 0.2 * newBullet.speed;
					mBullets.add(newBullet);
					
					newBullet = new Bullet();
					newBullet.x = mShip.x + (mShip.Laius / 2);
					newBullet.y = mShip.y;
					mBullets.add(newBullet);
					
					newBullet = new Bullet();
					newBullet.x = mShip.x + (mShip.Laius / 2);
					newBullet.y = mShip.y;
					newBullet.horizontalspeed = (float) -0.2 * newBullet.speed;
					mBullets.add(newBullet);
					
					newBullet = new Bullet();
					newBullet.x = mShip.x + (mShip.Laius / 2);
					newBullet.y = mShip.y;
					newBullet.horizontalspeed = (float) -0.4 * newBullet.speed;
					mBullets.add(newBullet);					
				}			
					
				else if (Parameters.mainWeapon == Parameters.WEAPON_MACHINEGUN) {
					newBullet = new Bullet();
					newBullet.x = mShip.x + (mShip.Laius / 2);
					newBullet.y = mShip.y;
					mBullets.add(newBullet);

					
					newBullet = new Bullet();
					newBullet.x = mShip.x + (mShip.Laius / 2);
					newBullet.y = mShip.y + 7;
					mBullets.add(newBullet);
					mBulletSound.start();
					
					newBullet = new Bullet();
					newBullet.x = mShip.x + (mShip.Laius / 2);
					newBullet.y = mShip.y + 14;
					mBullets.add(newBullet);
					mBulletSound.start();
				}
				else {//tavaline p�ss
					newBullet = new Bullet();
					newBullet.x = mShip.x + (mShip.Laius / 2);
					newBullet.y = mShip.y;
					mBullets.add(newBullet);
					mBulletSound.start();
				}
			}			
			
			// k�ib l�bi meie bulletite listi
			for(int i = 0; i < mBullets.size(); i++) {
				Bullet tmpBullet = mBullets.get(i);
				tmpBullet.y -= tmpBullet.speed;
				tmpBullet.x += tmpBullet.horizontalspeed;
				if(tmpBullet.y < 0) {
					mBullets.remove(i);
				}
				// k�ib l�bi vaenlaste listi
				for(int j = 0; j < mEnemies.size(); j++) {
					Enemy tmpEnemy = mEnemies.get(j);
					if(tmpBullet.x - tmpBullet.r < tmpEnemy.x + tmpEnemy.width && tmpBullet.x + tmpBullet.r > tmpEnemy.x && tmpBullet.y  - tmpBullet.r < tmpEnemy.y && tmpBullet.y + tmpBullet.r > tmpEnemy.y - tmpEnemy.height) {
						// meie kuuli (v�i nuke) kokkup�rge vaenlasega
						if(tmpBullet instanceof Nuke) {
							mNukeExplosion  = new NukeExplosion(tmpBullet.x, tmpBullet.y);
						}
						mBullets.remove(i);
						if(tmpEnemy.enemyShield < 1) {
							dropPowerUp(tmpEnemy.x, tmpEnemy.y);							
							mEnemies.remove(j);						
							Parameters.score += Parameters.level*tmpEnemy.getExp();
							playExplosionSound();
						}
						else {
							tmpEnemy.enemyShield --;
							playRickochetSound();
						}
						
					}
				}
			}
			
			if(mNukeExplosion != null) {
				mNukeExplosion.nukeR += mNukeExplosion.step;
				if(mNukeExplosion.nukeR > mNukeExplosion.maxR){
					mNukeExplosion = null;
				}
			}
			
			Parameters.enemyCount = mEnemies.size();
			
			// k�ib l�bi vaenlaste listi
			// kasutan iteraatorit, sest Exception tuli kui foreach ts�klis 
			// mEnemies listi elemente removisin
			for (Iterator<Enemy> iter = mEnemies.iterator(); iter.hasNext();) {
				Enemy enemy = iter.next();
				//paha laeva collison explosioniga	
				if(mNukeExplosion != null && 
						IsIntersected(new Point(
						Math.round(mNukeExplosion.nukeX), Math.round(mNukeExplosion.nukeY)), 
						mNukeExplosion.nukeR,
						Math.round(enemy.x), Math.round(enemy.y), 
						Math.round(enemy.width), Math.round(enemy.height))) {
					if(enemy.enemyShield < 1) {
						dropPowerUp(enemy.x, enemy.y);
						iter.remove();
						
						playExplosionSound();
					}
					else {
						enemy.enemyShield --;
						playRickochetSound();
					}
					
				}
				
				// paha laeva collision meie laevaga
				if(isIntersectedRectRect(enemy.x, enemy.y, enemy.width, enemy.height,
						mShip.x, mShip.y, mShip.Laius, mShip.Laius)) {
					iter.remove();					
					death();
					break;					
				}
				// paha laeva collision paddinguga
				if (enemy.y > getHeight() - SHIP_BOTTOM_PADDING) {
					iter.remove();
					playExplosionSound();
					Parameters.appState = AppState.GameOver;
					break;
				}
				if (enemyCanShoot) {
					Random generator = new Random();
					int roll = generator.nextInt(Parameters.ENEMY_SHOOTING_PROBABILITY);
					if (roll == 0) {
						// kui saab lasta, siis teeme �he paha kuuli
						Bullet enemyBullet = new Bullet();
						enemyBullet.x = enemy.x + enemy.width / 2;
						enemyBullet.y = enemy.y;
						if(enemy.isbadass) {
							enemyBullet.setBadass(true);
							enemyBullet.r = 3;
						}
						mEnemyBullets.add(enemyBullet);						
						enemyCanShoot = false;
						break;
					}
				}
			}
			
			// k�ib l�bi vaenlaste kuulide listi
			for(int i = 0; i < mEnemyBullets.size(); i++) {
				Bullet tmpBullet = mEnemyBullets.get(i);
				tmpBullet.y += tmpBullet.speed;
				if(tmpBullet.isBadass()) {
					if(tmpBullet.x < mShip.x + mShip.Laius/2) {
						tmpBullet.x += Parameters.BADASS_BULLET_HORIZONTAL_SPEED;
					}
					else {
						tmpBullet.x -= Parameters.BADASS_BULLET_HORIZONTAL_SPEED;
					}						
				}					
				if(tmpBullet.y > getHeight()) {
					mEnemyBullets.remove(i);
				}
				// paha kuuli collision meie laevaga
				if(tmpBullet.x - tmpBullet.r < mShip.x + mShip.Laius && tmpBullet.x + tmpBullet.r > mShip.x) {
					if (tmpBullet.y - tmpBullet.r > mShip.y && 
							tmpBullet.y < getHeight() - SHIP_BOTTOM_PADDING) {
						mEnemyBullets.remove(i);
						death();
						break;
					}
				}
			}
		}
		
		mRedrawHandler.sleep(SLEEP_TIMEOUT);
		
		// meie laeva surnud olemuse aja counter
		if (mShip.bleeding) {
			bleedingCounter += SLEEP_TIMEOUT;
			if (bleedingCounter > Parameters.BLEEDING_INTERVAL) {
				bleedingCounter = 0;
				mShip.bleeding = false;
			}
		}
		
		// meie tulistamise counteri kontroll
		if (!canShoot) {
			canShootCounter += SLEEP_TIMEOUT;
			if (canShootCounter > Parameters.SHOOT_INTERVAL) {
				canShootCounter = 0;
				canShoot = true;
			}
		}
		// meie tuumaka laskmise kontroll
		if (!canShootNuke) {
			canShootNukeCounter += SLEEP_TIMEOUT;
			mNukePicIndex = (int) Math.ceil(canShootNukeCounter / (Parameters.SHOOT_NUKE_INTERVAL / 6));
			if(mNukePicIndex < 0 || mNukePicIndex > 6) {
				mNukePicIndex = 0;
				canShootNukeCounter = 0;
				canShootNuke = true;
			}
		}
		
		// vaenlaste list �ritab tulistada
		if (!enemyCanShoot) {
			enemyCanShootCounter += SLEEP_TIMEOUT;
			if (enemyCanShootCounter > 500) {
				enemyCanShootCounter = 0;
				enemyCanShoot = true;
			}
		}
	}
	
	private void death() {
		if (!mShip.bleeding) {
			playExplosionSound();
			if(Parameters.lives > 0) {
				Parameters.lives --;
				mShip.bleeding = true;				
			}
			else {
				Parameters.appState = AppState.GameOver;
				canShootNuke = false;
			}
		}
	}

	private boolean isIntersectedRectRect(
			float rect1X, float rect1Y, float rect1Width, float rect1Height,
			float rect2X, float rect2Y, float rect2Width, float rect2Height) {
		
		if(rect1X < rect2X + rect2Width && 
				rect1X + rect1Width > rect2X && 
				rect1Y < rect2Y && 
				rect1Y + rect1Height > rect2Y) {
			return true;
		}
		else {
			return false;
		}
			
	}

	private void dropPowerUp(float x, float y) {
		PowerUp pup = null;
		Random generator = new Random();
		
		if (generator.nextInt(Parameters.DUAL_BULLET_DROP_PROBABILITY) == 0) {
			pup = new DualBulletWeaponPowerUp(dualWeaponPowerupImage);			
			}
		else if (generator.nextInt(Parameters.SPLASHER_DROP_PROBABILITY) == 0) {
			pup = new SplasherBulletWeaponPowerUp(splasherWeaponPowerupImage);
			}
		else if (generator.nextInt(Parameters.MG_DROP_PROBABILITY) == 0) {
				pup = new MgBulletWeaponPowerUp(machinegunWeaponPowerupImage);
			}
		else if (generator.nextInt(Parameters.LIFE_DROP_PROBABILITY) == 0) {
			pup = new LivesPowerUp(lifePowerupImage);
			}
		
		if (pup != null) {
			pup.x = x;
			pup.y = y;
			mPowerUps.add(pup);
		}
	}
	
	// t�hjendab k�ik listid, lisab levelile +1 ja teeb uued tulnukad
	private void goLevelUp() {
		Parameters.level++;
		
		mBullets.clear();
		mEnemies.clear();
		mEnemyBullets.clear();
		mNukeExplosion = null;
		//mPowerUps.clear();
		
		float stepX, stepY;
		float maxR, maxC;
		if(getWidth() < getHeight()) {				
			//portrait mode	
			maxC = 6;
			maxR = 8;					
			}
		else {
			//landscape mode
			maxC = 12;
			maxR = 4;
		}
		
		stepX = (getWidth() - PADDING) / maxC;
		stepY = (getHeight() - PADDING - getHeight() / 3) / maxR;
		//Parameters.enemySpeed += Parameters.level * 0.25;
		Parameters.enemySpeed += Parameters.getLevelupEnemySpeedAdjustment();
		populateEnemies(maxR, maxC, stepX, stepY, Parameters.INITIAL_ENEMY_COUNT);
	}
	
	// riistk��liku kokkupuute kontroll ringiga
	private static boolean IsIntersected(Point circle, float radius, 
			int rectangleX, int rectangleY, int rectangleWidth, int rectangleHeight) {
		
		Point rectangleCenter =
			new Point((rectangleX +  rectangleWidth / 2),
					(rectangleY + rectangleHeight / 2));

		float w = rectangleWidth  / 2;
		float h = rectangleHeight / 2;
  
		float dx = Math.abs(circle.x - rectangleCenter.x);
		float dy = Math.abs(circle.y - rectangleCenter.y);
		
		if (dx > (radius + w) || dy > (radius + h)) {
			return false;
		}
		
		float tmpX = Math.abs(circle.x - rectangleX - w);
		float tmpY = Math.abs(circle.y - rectangleY - h);
		Point circleDistance = new Point(Math.round(tmpX), Math.round(tmpY));
		
		if (circleDistance.x <= (w)) {
			return true;
		}
		
		if (circleDistance.y <= h) {
			return true;
		}
		
		double cornerDistanceSq = Math.pow(circleDistance.x - w, 2) +
			Math.pow(circleDistance.y - h, 2);
		
		return (cornerDistanceSq <= (Math.pow(radius, 2)));
	}
	
	//------------------------------------------------
	// placeholder classes
	private class Ship {
		public float x;
		public float y;
		public int movingSpeed;
		public int Laius;
		public Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		public boolean bleeding = true;
		
		public Ship() {
			paint.setColor(Color.GREEN);	
		}
	}
	
	private class Bullet {
		public float r;
		public float x,y;
		public float speed;
		public float horizontalspeed;
		private boolean badass = false;
		
		
		public Bullet() {
			r = 2;
			speed = 5;
			horizontalspeed = 0;
		}

		public void setBadass(boolean badass) {
			this.badass = badass;
		}
		public boolean isBadass() {
			return badass;
		}
	}
	
	private class Nuke extends Bullet {
		public Nuke() {
			super();
			r = 8;
		}
	}
	
	private class Enemy {
		public Paint paint;
		public float width, height;
		public float x,y;
		public float speed;
		Drawable image = null;
		public boolean isbadass = false;
		public float enemyShield = 0;
		
		public void draw(Canvas canvas) {
			image.draw(canvas);
		}
		
		public int getExp() {
			if(isbadass) {
				return 5;
			}
			else 
				return 1;
		}
		public Enemy(float speed, Drawable image) {
			if(image.equals(badassImage)) {
				isbadass = true;
			}
			this.image = image;
			this.speed = speed;
			width = 20;
			height = 10;
			this.speed = speed;
			paint = new Paint();
			paint.setColor(Color.rgb(0x255, 0x165, 0x000));
			paint.setAntiAlias(true);
		}
	}
	
	private abstract class PowerUp {
		public float height = 10;
		public float width = 10;
		Drawable image = null;
		public float x,y;
		public float speed = 3;
		
		public PowerUp (Drawable image) {
			this.image = image;
		}
		
		public void draw(Canvas canvas) {
			image.draw(canvas);
		}
	}
	
	private class DualBulletWeaponPowerUp extends PowerUp {
		public DualBulletWeaponPowerUp(Drawable image) {
			super(image);
		}
	}
	
	private class SplasherBulletWeaponPowerUp extends PowerUp {

		public SplasherBulletWeaponPowerUp(Drawable image) {
			super(image);
		}
	}
	
	private class MgBulletWeaponPowerUp extends PowerUp {

		public MgBulletWeaponPowerUp(Drawable image) {
			super(image);
		}
	}
	private class LivesPowerUp extends PowerUp {

		public LivesPowerUp(Drawable image) {
			super(image);
		}
	}
	private class NukeExplosion {
		public float nukeX, nukeY, nukeR, maxR=50, step=3;
		public NukeExplosion(float x, float y) {
			nukeX = x; 
			nukeY = y;
			nukeR = 8;
		}
	}
	
	private void shootNukeFromShipCoordinates() {
		if (mShip.bleeding) {
			return;
		}
		canShootNuke = false;
		mNuke = new Nuke();
		mNuke.x = mShip.x +mShip.Laius/2;
		mNuke.y = mShip.y - mShip.Laius;
		mBullets.add(mNuke);
		nukeLaunchSound.start();
	}
	
	public void resume() {
		Parameters.appState = AppState.Running;
	}
	
	public void reset() {
		clearLists();
		Parameters.score = 0;
		Parameters.level = 1;
		Parameters.mainWeapon = Parameters.WEAPON_SINGLEBULLET; 
		//Parameters.lives = .. // can't initialize here, Iceman wants 
		// different number of initial lives than some other guy. initial life count is set in params init
		Parameters.initialOnSizeChanged = true;
		Parameters.appState = AppState.Starting;
		init(this.getWidth(), this.getHeight());
	}
	
	public void clearLists() {
		mBullets.clear();
		mEnemies.clear();
		mEnemyBullets.clear();
		mPowerUps.clear();
	}
	
	//------------------------------------------------------
	
	
	// need 3 booleanit on hetkel allavajutatud nuppude kaardistamiseks
	boolean leftArrowIsDown = false;
	boolean rightArrowIsDown = false;
	boolean upArrowIsDown = false;
		
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if(keyCode == KeyEvent.KEYCODE_MENU || keyCode == KeyEvent.KEYCODE_BACK) {
			if (Parameters.appState != AppState.GameOver) {
				Parameters.appState = AppState.Paused;
			}
			et.stopMusic();
			((et) mContext).initMenus();
		}
		if(keyCode == KeyEvent.KEYCODE_DPAD_DOWN && canShootNuke) {
			shootNukeFromShipCoordinates();
		}
		if(keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
			leftArrowIsDown = true;
		}
		if(keyCode == KeyEvent.KEYCODE_DPAD_UP) {
			upArrowIsDown = true;
		}
		if(keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
			rightArrowIsDown = true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
			leftArrowIsDown = false;
		}
		if(keyCode == KeyEvent.KEYCODE_DPAD_UP) {
			upArrowIsDown = false;
		}
		if(keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
			rightArrowIsDown = false;
		}
		return super.onKeyUp(keyCode, event);
	}
	
	public boolean onTouch(View v, MotionEvent event) {
		if (Parameters.appState == AppState.GameOver) {
			((et) mContext).initMenus();
		}
		else {
			float touchX = event.getRawX();
			float touchY = event.getRawY();
			if (touchY < 30 && touchX > getWidth() - 30 && canShootNuke) {
				shootNukeFromShipCoordinates();
			}
			
			else if (touchY < getHeight()*2/3) {
				if(!mShip.bleeding && canShoot) {
					canShoot = false;
					Bullet bullet = new Bullet();
					bullet.x = mShip.x + mShip.Laius/2;
					bullet.y = mShip.y;
					mBullets.add(bullet);
				}
			}
			else {
				if (touchX - mShip.x < 0) {
					mShip.x = mShip.x - mShip.movingSpeed;
				}
				else /*if (touchX - mShip.getX() > 0)*/ {
					mShip.x = mShip.x + mShip.movingSpeed;
				}
			}
		}
		return true;
	}
}
