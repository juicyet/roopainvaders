package ee.home.roopa;

public enum AppState {
	Starting,
	Running,
	Paused,
	GameOver,
	Terminated
}
